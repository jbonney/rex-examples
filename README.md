# REX-examples
A collection of docker-compose files that allow testing different configurations of Remote Execution clients, execution services, CAS, Remote workers, etc.

 * Example 1: 
    - client: recc
    - Execution service: buildgrid
    - CAS/Action Cache: bb-storage
    - Workers: reccworker
