# Example 1
This sets up buildgrid/bb-storage/reccworker and also creates a recc container that can be interactively used.

To run a single instance of each run `docker-compose run recc` to bring up all the containers and run inside the `recc` container.

To run multiple workers, you can do
* `docker-compose up --scale reccworker=NUM --detach`
* `docker attach example1_recc_1` to attach to the running recc container

